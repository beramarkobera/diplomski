import React, {Component} from 'react';

import Header from './Header/Header';
import Workarea from './Workarea/Workarea';
import Footer from './Footer/Footer';

import {connect} from 'react-redux';
import * as actionCreators from './../../../store/actions';

import './Frame.css';

class Frame extends Component {
    componentDidMount() {
        this.props.onGetLoggedUser();
    }

    render() {
        return (
            <div className='Frame container'>
                <Header/>
                <Workarea />
                <Footer />
            </div>
        )
    }
    
}

const mapStateToProps = state => {
    return {
        user: state.usrs.user
    }
}

const mapDispatchToProps = dispatch => {
	return {
		onGetLoggedUser: () => dispatch(actionCreators.getLoggedUser())
	}
}

export default connect(mapStateToProps, mapDispatchToProps) (Frame);
