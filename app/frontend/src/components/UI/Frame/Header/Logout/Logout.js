import React from 'react';
import {Link} from 'react-router-dom';

import {connect} from 'react-redux';
import * as actionCreators from './../../../../../store/actions';

import './../Header.css';

const Logout = (props) => {
    return (
  
            <button
                className='Logout btn-fill' 
                onClick={() => {
                    console.log('LOGOUT')
                    // props.onLogout(); 
                    // props.onUnauthorizeUser()
                }
            }>
                <Link to='/'>Odjavljivanje</Link>                    
            </button>

       
    )
}

export const mapDispatchToProps = dispatch => {
    return {
        // onLogout: () => dispatch(actionCreators.logout()),
        // onUnauthorizeUser: () => dispatch(actionCreators.unauthorizeUser())
    }
}

export default connect(null, mapDispatchToProps) (Logout);

