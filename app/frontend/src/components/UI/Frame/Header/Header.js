import React from 'react';
import { Link } from 'react-router-dom';

import { connect } from 'react-redux';
import * as actionCreators from './../../../../store/actions';

import HeaderLink from './HeaderLink/HeaderLink';

import user from '../../../../assets/user.svg';
import exit from '../../../../assets/exit.svg';

import './Header.css';

const Header = (props) => {

	return (
		<header className='Header flex-container'>
			<HeaderLink 
				text={props.name} 
				style='Header btn-link' 
				path='/licni_podaci'
				icon={user}/>
			<div className='Header spacer'></div>
			<HeaderLink 
				text='Logout' 
				style='Header btn-link' 
				path='/' 
				// act={props.onLogout, props.onUnauthorizeUser }
				icon={exit}/>	
		</header>
	)
}

export const mapStateToProps = state => {
	return {
		name: state.usrs.user.f_name + ' ' + state.usrs.user.l_name
	}
}

export const mapDispatchToProps = dispatch => {
	return {
		// onLogout: () => dispatch(actionCreators.logout()),
		// onUnauthorizeUser: () => dispatch(actionCreators.unauthorizeUser()),
		onTest: () => dispatch(actionCreators.startTest())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
