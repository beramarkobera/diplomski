import React from 'react';
import { Link } from 'react-router-dom';

import './../Header.css';

const HeaderLink = (props) => {
	return (	
		<div className={props.style}>
			<button onClick={ props.act }>
				<img src={props.icon} alt='Something wrong'/>
				<Link to={props.path}> {props.text}</Link>
			</button>
		</div>
	)
}

export default HeaderLink;