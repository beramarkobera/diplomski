import React from 'react';

import Sidebar from './Sidebar/Sidebar';
import Main from './Main/Main';

import './Workarea.css';

const Workarea = (props) => {
    return (
        <div className='Workarea container'>
            <Sidebar />
            <Main />            
        </div>
    )
}

export default Workarea
