import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import './NavItem.css';


class NavItem extends Component {
    state = {
        toggle: false
    }

    toggleHandler = () => {
        console.log(this.state.toggle)
        this.setState( (prevState) => {
            return {toggle: !prevState.toggle};
        });
    }

    render() {
        const {linkTo, title, children} = this.props.info;
        let descendants = null;
        if(this.state.toggle)
            if(children) {
                descendants = children.map( item => {
                    return <div key={item.title} className='nav container'>
                        <NavItem info={item} />
                    </div>
                })
            }

        const btn = children ? 
            <button onClick={this.toggleHandler} className='NavItem btn-fill'> 
                {title} 
            </button> : 
            <button className='NavItem btn-fill'>
                <Link to={'/'+linkTo} className='NavItem anchor-fill'> {title} </Link>
            </button>

        return (
            <div >
                <div className='nav container'>                    
                    {btn}
                </div>
                
                
                <div>
                    {descendants}
                </div>
                
            </div>
            
        )
    };
    
}

export default NavItem
