import React from 'react';

import {connect} from 'react-redux';
//import * as actionCreators from './../../../../../store/actions';

import './Sidebar.css';

import NavItem from './NavItem/NavItem';

const Sidebar = (props) => {

    const its = props.menuItems ? props.menuItems.map( item => {
        return <div key={item.title}> <NavItem info={item} /> </div>
    }) : null;

    const items = <div className='sidebar ul'>
        {its}
    </div>

    return (
        <div className='Sidebar container'>
            {items}
        </div>
    )    
}

const mapStateToProps = state => {
    let value = null;
    if(state.usrs.menu.length > 1)
        value = state.usrs.menu
    return {
        menuItems: value
    }
}

export default connect(mapStateToProps) (Sidebar);