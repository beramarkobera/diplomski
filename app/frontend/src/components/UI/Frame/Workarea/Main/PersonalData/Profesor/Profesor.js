import React from 'react';
import {Link} from 'react-router-dom';

const Profesor = (props) => {
    console.log(props)
    const {index, f_name, l_name, email} = props.profesor;
    const {country, city, street} = props.profesor.address;

    return (
        <div>
            <h2> {f_name + ' ' + l_name} <br/> {index} </h2>

            <p> 
                <label> Email adresa: </label> {email}
            </p>
            <p> 
                <label> Adresa stanovanja: </label> {`${street}, ${city}, ${country}`}
            </p>
            <br/>

            <Link to='/licni_podaci/izmena'><button > Izmena podataka </button> </Link>
            
        </div>
    )
    /*return (
        <div>
            hahah
        </div>
    )*/
}

export default Profesor
