import React, {Component} from 'react';

import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import * as actionCreators from '../../../../../../../store/actions';

import ApplyExam from './ApplyExam';

class ExamApplication extends Component {

    componentDidMount() {
        console.log(this.props.exam_schedule)
        this.props.onClearSubjects();
        this.props.subIDs.map( item => {
            if('NEPOLOZEN' === item.status) {               
                this.props.onGetSubject(item.sub_id)
            }
        })
    }

    render() {

        const exams = this.props.subs.map( item => {
            return <li key={item.sub_id}>
                {item.name}  
                <button onClick={() => this.props.onSubmitApplication(item.sub_id, this.props.user_email, this.props.chosenExamDateId)}> Prijava ispita</button>
            </li>
        })

        return (
            <div>
                {exams}
            </div>
        )
    }   
}

export const mapStateToProps = state => {
    return {
        subIDs: state.usrs.user.subjects,
        user_email: state.usrs.user.email,
        subs: state.subs.subjects,
        chosenExamDateId: state.exams.chosenExamDateId
    }
}
export const mapDispatchToProps = dispatch => {
    return {
        onGetSubject: (sub_id) => (dispatch(actionCreators.getSubject(sub_id))),
        onClearSubjects: () => dispatch(actionCreators.clearSubjects()),
        onSubmitApplication: (sub_id, user_email, exam_schedule) => dispatch(actionCreators.submitApplication(sub_id, user_email, exam_schedule)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (ExamApplication)
