import React, {Component} from 'react';

import {connect} from 'react-redux';
import * as actionCreators from '../../../../../../store/actions';

const Subject = (props) => {

    const currentSubject = props.subjects.filter( sub => {
        return sub.sub_id === props.match.params.sub_id;
    })

    console.log(currentSubject)
    return (
        
        <div>
            <p>
                {currentSubject[0].name}
            </p>
        </div>
    )
    
}

const mapStateToProps = state => {
    return {
        subjects: state.subs.subjects,
        user: state.usrs.user
    }
}

export default connect(mapStateToProps) (Subject);
