import React, {Component} from 'react';

import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import * as actionCreators from '../../../../../../../store/actions';

import Subject from './../Subject';

class StudentSubjects extends Component {

    componentDidMount() {
        this.props.onClearSubjects();
        this.props.subjects.map( item => {
            if(this.props.status === item.status) {               
                this.props.onGetSubject(item.sub_id)
            }
        })
    }

    render() {
        let {subs} = this.props;

        let subjects = subs ? subs.map( item => {
            return (
                <li key={item.sub_id}>
                    <Link to={`/predmeti/${item.sub_id}`} > 
                            {item.name} 
                    </Link>
                </li>
            )           
        }) : null ;

        return (
            <div>
                
                <ol>
                    {subjects}
                </ol>
            </div>
        )
    }
    
}

const mapStateToProps = state => {
    return {
        subs: state.subs.subjects,
        subIDs: state.usrs.user.subjects
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onGetSubject: (sub_id) => (dispatch(actionCreators.getSubject(sub_id))),
        onClearSubjects: () => dispatch(actionCreators.clearSubjects())
    }
}


export default connect(mapStateToProps, mapDispatchToProps) (StudentSubjects)
