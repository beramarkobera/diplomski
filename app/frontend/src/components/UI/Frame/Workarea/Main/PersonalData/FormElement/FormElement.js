import React from 'react';

import './FormElement.css';

const FormElement = (props) => {

    const element = setElement(props.label, props.val, props.change);

    return (
        <div className='FormElement wrapper'>
            {element}
        </div>
    )
}

export default FormElement;

const setElement = (text, value, handleChange) => {

    let label = null;
    switch (text) {
        case 'f_name': 
            label = 'Ime ';
            break;
        case 'l_name':
            label = 'Prezime '
            break;
        case 'phone':
            label = 'Telefon '
            break;
        case 'address':
            label = 'Adresa '
            break;
        default:
            label = 'Something wrong'
    }
    const elem = text === 'address' ? 
        <div className='FormElement container'>
            <div>
                <br/>
                <h2 className='FormElement address'>Adresa: </h2>
                <br/>
                <label> <span> Država: </span>  
                    <input 
                        placeholder='Država...'
                        value={value.country}
                        onChange={(e) => handleChange('country', e.target.value)}
                    />
                </label>
                <label> <span> Grad: </span> 
                    <input 
                        placeholder='Grad...'
                        value={value.city}
                        onChange={(e) => handleChange('city', e.target.value)}
                    />
                </label>
                <label> <span>Ulica i broj:</span> 
                    <input 
                        placeholder='Ulica i broj...'
                        value={value.street}
                        onChange={(e) => handleChange('street', e.target.value)}
                    />
                </label>
            </div>
            
        </div> :
        <div className='FormElement container'>
            <label> <span> {label}: </span>    
                <input 
                    placeholder={label + '...'} 
                    value={value}
                    onChange={(e) => handleChange(text, e.target.value)}
                />
            </label>
        </div>
    
    return elem;
} 
