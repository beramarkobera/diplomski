import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as actionCreators from './../../../../../../../store/actions';

import FormElement from './../FormElement/FormElement';

import './Student.css';

class ChangeStudent extends Component {
    state = {
        f_name: '',
        l_name: '',
        phone: '',
        country: '',
        city: '',
        street: ''
    }

    componentDidMount() {
        this.setState({
            f_name: this.props.user.f_name,
            l_name: this.props.user.l_name,
            phone: this.props.user.phone,
            country: this.props.user.address.country,
            city: this.props.user.address.city,
            street: this.props.user.address.street
        })
    }

    handleChange = (key,value) => {
        console.log(key)
        this.setState({
            [key]: value
        });
    }
    
    render() {
        return (
            <form className='Student form-container' onSubmit={(e) => {e.preventDefault(); this.props.onUpdateStudent(this.state)}}>           
                <FormElement label='f_name' val={this.state.f_name} change={this.handleChange}/>
                <FormElement label='l_name' val={this.state.l_name} change={this.handleChange}/>
                <FormElement label='phone' val={this.state.phone} change={this.handleChange}/>
                
                <FormElement 
                    label='address' 
                    val={this.state} 
                    change={this.handleChange}
                />

                <button className='Student submit-change'> Promeni podatke </button>
                <button onClick={(e) => {e.preventDefault(); console.log(this.state)}}> Logger</button>
            </form>
        )
    }
    
}

const mapStateToProps = state => {
    return {
        user: state.usrs.user
    }
}

const mapDispatchToProps = dispatch => {
    return {
		onUpdateStudent: (info) => dispatch(actionCreators.updateStudent(info))
	}
}

export default connect(mapStateToProps, mapDispatchToProps) (ChangeStudent);
