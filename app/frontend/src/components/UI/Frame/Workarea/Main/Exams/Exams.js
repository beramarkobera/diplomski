import React, {Component} from 'react';
import moment from 'moment';

import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import * as actionCreators from '../../../../../../store/actions';
import {parseCurrentDate} from '../../../../../../utility/utility';

class Exams extends Component {
    /*let component = null;
    if(props.usr)
        switch(props.usr.role) {
            case roles.STUDENT: {
                return component = <StudentSubjects subjects={props.usr.subjects} status={props.status}/>
            }
            case roles.PROFESOR: {
                return component = <ProfesorSubjects subjects={props.usr.subjects}/>
            }
            default:
                break;
        }*/
    state = {
        currentDate: moment(parseCurrentDate(), "DD/MM/YYYY")
    }

    componentDidMount() {
        this.props.onGetExamDates();
    }

    render() {
        const {examDates} = this.props;
        const examsList = examDates ? 
            examDates.examDates.map( item => {

                let begin = moment(item.start, "DD/MM/YYYY");
                let end = moment(item.end, "DD/MM/YYYY");
                if(this.state.currentDate >= begin && this.state.currentDate < end)
                    return <li key={item._id}>
                        <Link to={`/prijava_ispita/${item.link}`}> 
                            <button onClick={() => this.props.onSelectedExam(item._id)}> {item.name} </button>
                        </Link>
                    </li>

                
            }) : null
        return (
            <div>
                Exams
                <ol>
                    {examsList}
                </ol>
            </div>
        )
    }
    
}

export const mapStateToProps = state => {
    return {
        examDates: state.exams.examDates
    }
}
export const mapDispatchToProps = dispatch => {
    return {
        onGetExamDates: () => dispatch(actionCreators.getExamDates()),
        onSelectedExam: (schedule_id) => dispatch(actionCreators.selectExam(schedule_id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (Exams);
