import React from 'react';
import {connect} from 'react-redux';

import * as roles from '../../../../../../userRoles';
import * as actionCreators from '../../../../../../store/actions';

import StudentSubjects from './Student/StudentSubjects';
import ProfesorSubjects from './Profesor/ProfesorSubjects';


const Subjects = (props) => {
    let component = null;
    if(props.usr)
        switch(props.usr.role) {
            case roles.STUDENT: {
                return component = <StudentSubjects subjects={props.usr.subjects} status={props.status}/>
            }
            case roles.PROFESOR: {
                return component = <ProfesorSubjects subjects={props.usr.subjects}/>
            }
            default:
                break;
        }

    return (
        <div>

        </div>
    )
}

const mapStateToProps = state => {
    return {
        usr: state.usrs.user
    }
}


export default connect( mapStateToProps) (Subjects);
