import React from 'react';
import {connect} from 'react-redux';

import Student from './Student/Student';
import Profesor from './Profesor/Profesor';
import * as roles from '../../../../../../userRoles';

const PersonalData = (props) => {
    console.log(props.usr)
    let component = null;
    if(props.usr)
        switch(props.usr.role) {
            case roles.STUDENT: {
                return component = <Student student={props.usr}/>
            }
            case roles.PROFESOR: {
                return component = <Profesor profesor={props.usr}/>
            }
            default:
                break;
        }

    return (
        <div>
            {component}
        </div>
    )
}

const mapStateToProps = state => {
    return {
        usr: state.usrs.user
    }
}

export default connect(mapStateToProps) (PersonalData);
