import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';

import PersonalData from './PersonalData/PersonalData';
import ChangeStudent from './PersonalData/Student/ChangeStudent';
import Subjects from './Subjects/Subjects';
import Subject from './Subjects/Subject';
import Exams from './Exams/Exams';
import ProfesorSubjects from './Subjects/Profesor/ProfesorSubjects';
import ExamApplication from './Exams/Student/ExamApplication';


class Main extends Component {

    render() {
        return (
            <main>
                <Switch>
                    <Route path='/licni_podaci/izmena' component={ChangeStudent} />
                    <Route exact path='/licni_podaci' component={PersonalData} />
     
                    <Route 
                        path='/predmeti/polozeni' 
                        key='polozeni-predmeti'
                        render={props => <Subjects status='POLOZEN' {...props} />} />   
                    <Route 
                        path='/predmeti/nepolozeni' 
                        key='nepolozeni-predmeti'
                        render={props => <Subjects status='NEPOLOZEN' {...props} />} />

                    <Route path='/predmeti/povereni' component={Subjects}/>   
                
                    <Route path='/predmeti/:sub_id' component={Subject} />
                    <Route path='/prijava_ispita/:exam_link' component={ExamApplication} /> 
                    <Route path='/prijava_ispita' component={Exams} />     
                                 
                </Switch>                   
            </main>
        )
    }
}

export default Main
