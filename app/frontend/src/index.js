import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';

import {Provider} from 'react-redux';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import {BrowserRouter, Switch} from 'react-router-dom';
import thunk from 'redux-thunk';

import subjectsReducer from './store/reducers/subjectsReducer';
import usersReducer from './store/reducers/usersReducer';
import casReducer from './store/reducers/casReducer';
import examsReducer from './store/reducers/examsReducer';


const rootReducer = combineReducers({
    subs: subjectsReducer,
    usrs: usersReducer,
    cas: casReducer,
    exams: examsReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));


ReactDOM.render(  
    <Provider store={store}> 
        <BrowserRouter>
            <Switch>
                <App />
            </Switch>     
        </BrowserRouter>
    </Provider> , 
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
