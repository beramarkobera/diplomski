import React, {Component} from 'react';
import {Route} from 'react-router-dom';

import './App.css';

import Frame from './components/UI/Frame/Frame';
import {connect} from 'react-redux';
import * as actionCreators from './store/actions';

class App extends Component {
	componentDidMount() {
		this.props.onStartAuth();		
	}
	
	render() {
		const url = this.props.url ? this.props.url : null;
		const falseRoute = url ? 
				<Route 
					path='/' 
					component={() => { 
						console.log('Access denied. Go to CAS')
						window.location.replace(url);
						return null; 
					}
				}
				/>	:
				null
		const show = this.props.access ? <Frame show={this.props.access}/> : null;
		console.log('SHOW PAGE: ', show)

		return (
			<div className="App">
				{falseRoute}
				{show}
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		access: state.cas.authorized,
		url: state.cas.redirectUrl
	}
}
const mapDispatchToProps = dispatch => {
	return {
		onStartAuth: () => dispatch(actionCreators.startAuth())
	}
}

export default connect(mapStateToProps, mapDispatchToProps) (App);
