import * as at from './actionTypes';
import { CAS_SERVER_URL, BACKEND_URL } from './constants';

export const redirectTo = (link) => {
    return {
        type: at.CAS_TRANSFER_REQUEST,
        url: link
    }
}

export const validateTicket = () => {
    console.log('Ticket validation...')
    return { type: at.CAS_CHECK_TICKET }
}
export const checkAuthenticity = () => { 
    return { type: at.CHECK_AUTHENTICITY} 
}
export const redirectToCas = (redirectUrl) => { 
    console.log('Redirect to CAS...')
    return { 
        type: at.REDIRECT_TO_CAS, 
        url: redirectUrl 
    } 
}
export const allowUser = () => {
    return {
        type: at.ALLOW_USER,
        authorized: true
    }
}
export const forbidUser = () => {
    return {
        type: at.FORBID_USER,
        authorized: false
    }
}
export const unauthorizeUser = () => {
    return dispatch => {
        dispatch(forbidUser())
    }
}

export const startAuth = () => {
    const query1 = window.location.search.trim().substring(1);
    const parsedQuery = parseQueryString( query1 )
    console.log(parsedQuery)

    return async dispatch => {
        try {
            if(parsedQuery['ticket']) {
                console.log('We got ourselves a ticket!')
                dispatch(validateTicket());           
                let response = null;
                let data = null;
                console.log('Parsed query: ', parsedQuery['ticket'])
                response = await fetch(CAS_SERVER_URL + '/auth/serviceValidate?ticket=' + parsedQuery['ticket'], { credentials: 'include'} )
                data = await response.json();
                console.log('THIS IS A RESPONSE: ', data)
                

                if(response.status === 200) {
                    console.log('Cool!')
                    //console.log(data.msg.)
                    dispatch(allowUser());
                }
            
            }
                
            else {
                console.log('We have no ticket, folks!')
                dispatch(redirectToCas('http://localhost:3000/cas/login?service=http://localhost:3030'));
            } 
        } catch (err) {
            console.log(err)
            throw err
        }  
    }     
    
}

export const startTest = () => {
    return async dispatch => {
        try {
            const response = await fetch(BACKEND_URL + '/auth/test', { credentials: "include" })
            const data = await response.json();
        } catch (err) {
            console.log(err)
        }
    }
}


const parseQueryString = function( queryString ) {
    let params = {}, queries, temp, i, l;
    // Split into key/value pairs
    queries = queryString.split("&");
    // Convert the array of strings into an object
    for ( i = 0, l = queries.length; i < l; i++ ) {
        temp = queries[i].split('=');
        params[temp[0]] = temp[1];
    }
    return params;
};
