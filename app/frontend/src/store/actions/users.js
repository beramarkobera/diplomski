import * as actionTypes from './actionTypes';
import {BACKEND_URL} from './constants';

export const setUser = (data) => {
    return {
        type: actionTypes.GET_USER,
        user: data
    }
}

export const setMenu = (role) => {
    return {
        type: actionTypes.SET_MENU_ITEMS,
        menu: role
    }
}

// export const logoutUser = () => {
//     console.log('Logout')
//     return {
//         type: actionTypes.LOGOUT
//     }
// }

export const changeUserInfo = (updatedInfo) => {
    return {
        type: actionTypes.CHANGE_USER_INFO,
        updatedInfo: updatedInfo
    }
}

export const getUser = (email) => {
    return async dispatch => {
        let response = null;
        let data = null;
        try {
            response = await fetch(BACKEND_URL + '/users');
            data = await response.json();
            console.log(data.data)
        } catch (err) {
            throw err
        }
    }
}

export const getLoggedUser = () => {
    return async dispatch => {
        try {
            console.log('Getting logged in user!')

            const response = await fetch(BACKEND_URL + '/users/logged', { credentials: 'include' });
            const data = await response.json();
            console.log('Found User: ', data)
            dispatch(setUser(data.user));
            dispatch(setMenu(data.user.role))
            console.log(data)
        } catch (err) {
            throw err
        }
    }
}

// export const logout = () => {
//     return async dispatch => {
//         console.log('Logged out')
//         let response = null;
//         let data = null;
//         try {
//             response = await fetch(BACKEND_URL + '/users/logout', {
//                 credentials: 'include'
//             });
//             data = await response.json();
//         } catch (err) {
//             console.log(err)
//         }
// //        if(response.status === 200) {
//             console.log(data)
//             dispatch(logoutUser());
// //        }
//     }
// }

export const updateStudent = (info) => {
    return async dispatch => {
        console.log('User updated');
        let response = null;
        let data = null;
        dispatch(changeUserInfo(info))
        try {
            response = await fetch(BACKEND_URL + '/users/change_student', {
                method: 'POST',
                credentials: 'include',
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
            },
                body: JSON.stringify(info)
            });
            data = await response.json();
        } catch (err) {
            console.log(err)
            throw err
        }
    }
}