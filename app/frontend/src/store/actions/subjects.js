import * as actionTypes from './actionTypes';
import {BACKEND_URL} from './constants';

export const setSubject = (data) => {
    return {
        type: actionTypes.GET_SUBJECT,
        subject: data
    };
}

export const clearAll = () => {
    return { type: actionTypes.CLEAR_SUBJECTS }
}

export const getSubject = (id) => {
    console.log(id)
    return async dispatch => {    
        const query = `?subject=${id}`;

        let response = null;
        let data = null;

        try {
            response = await fetch(BACKEND_URL + '/subjects' + query, {credentials: 'include'});
            data = await response.json();
        } catch (err) {
            console.log(err)
        }

        dispatch(setSubject(data.subject))
    }
}

export const clearSubjects = () => {
    return dispatch => {
        dispatch(clearAll());
    }
}
