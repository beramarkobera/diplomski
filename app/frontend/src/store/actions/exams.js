import * as actionTypes from './actionTypes';
import {BACKEND_URL} from './constants';

export const fetchExamDates = (data) => {
    return {
        type: actionTypes.GET_EXAM_DATES,
        data: data
    }
}

export const getExamDates = () => {
    return async dispatch => {
        console.log('Dispatched, bro!');
        
        let response = null;
        let data = null;      
        try {
            response = await fetch(BACKEND_URL + '/exams/get_exam_dates' , {credentials: 'include'});
            data = await response.json();
        } catch (error) {
            console.log(error)
        }

        dispatch(fetchExamDates(data))
    }
}

export const submitApplication = (sub_id, user_email, schedule_id) => {
    return async dispatch => {
        console.log('application subbmited, sub_id: ' + sub_id + ', user: ' + user_email, ', schedule: ' + schedule_id);
        let response = null;
        let data = null;
        try {
            response = await fetch(BACKEND_URL + '/exams/submit_application', {
                method: 'POST',
                credentials: 'include',
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
            },
                body: JSON.stringify({
                    sub_id: sub_id,
                    user_email: user_email,
                    schedule_id: schedule_id
                })
            });
            data = await response.json();
        } catch (error) {
            console.log(error)
        }
    }
}

export const setSelectedExam = (exam_id) => {
    return {
        type: actionTypes.SET_SELECTED_EXAM,
        data: exam_id
    }
}

export const selectExam = (exam_id) => {

    return dispatch => {
        dispatch(setSelectedExam(exam_id));
    }
}

export const fetchExams = (fetchedExams) => {
    return {
        type: actionTypes.GET_EXAMS,
        data: fetchedExams
    }
}
export const getExams = () => {

    return async dispatch => {
        

        let response = null;
        let data = null;
        try {
            response = await fetch(BACKEND_URL + '/exams/get_exams', {credentials: 'include'});
            data = await response.json();
        } catch (error) {
            console.log(error);
        }

        dispatch(fetchExams(data));
    }

}