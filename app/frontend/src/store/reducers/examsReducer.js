import * as actionTypes from '../actions/actionTypes';

const initialState = {
    examDates: {
        examDates: []
    },
    chosenExamDate: {
        link: null,
        name: null,
        start: null,
        end: null
    },
    exams: [
        {
            _id: null,
            subject: null,
            scheduleLink: null,
            dateTime: null
        }
    ],
    chosenExamDateId: null
};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.GET_EXAM_DATES:
            return {
                examDates: action.data               
            }
        case actionTypes.GET_EXAMS:
            return {
                exams: action.data
            }
        case actionTypes.SET_SELECTED_EXAM:
            return {
                chosenExamDateId: action.data
            }
        default:
            return state;
    }
};

export default reducer;