import * as actionTypes from '../actions/actionTypes';

const initialState = {
    subjects: []
};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.GET_SUBJECT:
            return {
                subjects: [
                    ...state.subjects,
                    action.subject
                ]
            }
        case actionTypes.CLEAR_SUBJECTS:
            return {
                subjects: []
            }
        default:
            return state;
    }
};

export default reducer;