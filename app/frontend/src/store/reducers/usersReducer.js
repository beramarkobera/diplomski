import * as actionTypes from '../actions/actionTypes';
import * as menuItems from './menuItems.js';

const initialState = {
    permit: null,
    user: {
        index: null,
        f_name: null,
        l_name: null,
        email: null,
        phone: null,
        role: null,
        address: {
            country: null,
            city: null,
            street: null
        },
        subjects: []
    },
    menu: [
        { 
            title: null, 
            linkTo: null, 
            children: []
        }
    ]
}

const reducer = ( state = initialState, action ) => {
    switch(action.type) {
        case actionTypes.GET_USER:
            return {
                ...state,
                user: action.user
            }
        case actionTypes.LOGOUT:
            return {
                ...state,
                user: null
            }
        case actionTypes.SET_MENU_ITEMS:
            return {
                ...state,
                menu: menuItems[action.menu]
            }
        case actionTypes.CHANGE_USER_INFO:
            const {f_name, l_name, phone, country, city, street} = action.updatedInfo
            return {
                menu: [
                    ...state.menu
                ],
                user: {
                    ...state.user,
                    f_name: f_name,
                    l_name: l_name,
                    phone: phone,
                    address: {
                        country: country,
                        city: city,
                        street: street
                    }                      
                }
            }
        default:
            return state;
    }
};

export default reducer;