import * as at from '../actions/actionTypes';

const initialState = {
    //token: null,
    //location: null,
    //service: null, 
    authorized: null,
    redirectUrl: null
}

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case at.REDIRECT_TO_CAS: {
            return {
                ...state,
                redirectUrl: action.url
            }
        }
        case at.ALLOW_USER: {
            return {
                ...state,
                authorized: true
            }
        }
        case at.FORBID_USER: {
            return {
                ...state,
                authorized: false
            }
        }
        default:
            return state;
    }
}

export default reducer;