export const STUDENT = [
    { 
        title: 'Lični podaci', 
        linkTo: 'licni_podaci', 
        children: null
    },
    { 
        title: 'Predmeti', 
        linkTo: 'predmeti', 
        children: [
            { 
                title: 'Položeni predmeti', 
                linkTo: 'predmeti/polozeni', 
                children: null
            },
            { 
                title: 'Nepoloženi predmeti', 
                linkTo: 'predmeti/nepolozeni', 
                children: null
            }
        ]
    },
    { 
        title: 'Ispiti', 
        linkTo: 'Ispiti', 
        children: [
            { 
                title: 'Prijava ispita', 
                linkTo: 'prijava_ispita', 
                children: null
            },
            { 
                title: 'Prijavljeni ispiti', 
                linkTo: 'prijavljeni_ispiti', 
                children: null
            }
        ]
    }
];

export const PROFESOR = [
    { 
        title: 'Lični podaci', 
        linkTo: 'licni_podaci', 
        children: null
    },
    { 
        title: 'Predmeti', 
        linkTo: 'predmeti', 
        children: [
            { 
                title: 'Povereni predmeti', 
                linkTo: 'predmeti/povereni', 
                children: null
            }
        ]
    }
];