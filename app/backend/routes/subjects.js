const express = require('express');

const subjectsController = require('../controllers/subjects');

const router = express.Router();

router.get('/', subjectsController.getSubject);



module.exports = router;
