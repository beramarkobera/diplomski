const express = require('express');
const examsController = require('../controllers/exams');

const router = express.Router();

router.get('/get_exam_dates', examsController.getExamDates);
router.post('/submit_application', examsController.postSubmitApplication);
router.get('/get_exams', examsController.getExams)

module.exports = router;
