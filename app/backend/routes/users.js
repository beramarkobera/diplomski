const express = require('express')
const usersController = require('../controllers/users');
const router = express.Router();

router.get('/', usersController.getUser);
router.get('/logged', usersController.getLoggedUser)
router.get('/logout', usersController.logoutUser)

router.post('/change_student', usersController.changeStudent)


module.exports = router;