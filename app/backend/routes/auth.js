const express = require('express');

const authController = require('../controllers/auth');

const router = express.Router();

router.get('/get_access', authController.getProtectedApp);
router.post('/from-cas/', authController.fromCas);

router.get('/test', authController.test);



module.exports = router;
