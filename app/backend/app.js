const path = require('path');
const fs = require('fs');
const https = require('https');

const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');

const subjectsRoutes = require('./routes/subjects');
const usersRoutes = require('./routes/users');
const authRoutes = require('./routes/auth');
const examsRoutes = require('./routes/exams');

const privateKey = fs.readFileSync('localhost.key');
const certificate = fs.readFileSync('localhost.crt');

const app = express();

const port = process.env.PORT || 5000;

app.use(cookieParser());
app.use(bodyParser.json()); // application/json

app.use((req, res, next) => {
  //console.log(req.headers)
  //res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
  res.setHeader(
    'Access-Control-Allow-Methods',
    'OPTIONS, GET, POST, PUT, PATCH, DELETE'
  );
  res.setHeader('Access-Control-Allow-Credentials', true);
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});

app.use('/subjects', subjectsRoutes);
app.use('/users', usersRoutes);
app.use('/auth', authRoutes);
app.use('/exams', examsRoutes);

mongoose
  .connect(
    'mongodb://root:root123456@ds263856.mlab.com:63856/stud_app',
    { useNewUrlParser: true }
  )
  .then(result => {
    console.log(`Starting server on port ${port}`)
    //app.listen(port);
    https.createServer({ key: privateKey, cert: certificate}, app).listen(port)
  })
  .catch(err => console.log(err));
