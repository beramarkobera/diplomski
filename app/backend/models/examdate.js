const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const examdateSchema = new Schema(
    {
      name: {
        type: String,
        required: true
      },
      start: {
        type: String,
        required: true
      },
      end: {
        type: String,
        required: true
      }
    }
  );
  
  module.exports = mongoose.model('Exam_schedule', examdateSchema);
  