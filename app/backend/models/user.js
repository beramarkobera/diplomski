const mongoose  = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    f_name: { type: String, required: true },
    l_name: { type: String, required: true },
    index: { type: String, required: false },
    email: { type: String, required: true },
    phone: { type: String, required: false },
    address: {
        country: { type: String, required: true },
        city: { type: String, required: true },
        street: { type: String, required: true }
    }, 
    role: { type: String, required: true },
    subjects: [
        { 
            subject: { type: Object, required: false },
            status: { type: String, required: false }
        }
    ]

});

module.exports = mongoose.model('User', userSchema);