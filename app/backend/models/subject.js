const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const subjectSchema = new Schema(
    {
      sub_id: {
        type: String,
        required: true
      },
      name: {
        type: String,
        required: true
      },
      espb: {
        type: Number,
        required: true
      },
      year_of_study: {
        type: String,
        required: true
      }
    },
    { timestamps: true }
  );
  
  module.exports = mongoose.model('Subject', subjectSchema);
  