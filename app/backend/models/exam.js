const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const examSchema = new Schema({
    subject: {
        type: String,
        required: true
    },
    dateTime: {
        type: String,
        required: true
    },
    scheduleID: {
        type: String,
        required: true
    },
    userEmail: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Exam_application', examSchema);