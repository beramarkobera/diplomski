const User = require('../models/user');

exports.getUser = (req, res, next) => {
    User.findOne({"index": "RA151-2012"})
        .then( user => {
            res
                .status(200)
                .json( {
                    message: 'User successfully fetched',
                    data: user
                });
        })
        .catch( err => {
            console.log(err);
        })
};

exports.getLoggedUser = async (req, res, next) => {
    //console.log(req.cookies.SSOSESSION)
    console.log('GETTING LOGGED IN USER')
    const email = req.cookies.SSOSESSION.username;

    let loggedUser = null;
    try {
        loggedUser = await User.findOne({
            email: email
        })
    } catch (err) {
        return res.json({
            error: 'Something wrong while fetching logged in user'
        })
    }
    console.log(loggedUser)
    res.json({
        user: loggedUser
    });
    
}

exports.logoutUser = (req, res, next) => {
    console.log(req.cookies)
    res.clearCookie('SSOSESSION');
    res.json({
        message: 'Cookie cleared'
    })
}

exports.changeStudent = async (req, res, next) => {
    console.log(req.body)
    const email = req.cookies.SSOSESSION.username;
    const {f_name, l_name, phone, country, city, street} = req.body;

    let loggedUser = null;
    try {
        loggedUser = await User.findOneAndUpdate({
            email: email
        },
        {
            f_name: f_name,
            l_name: l_name,
            phone: phone,
            address: {
                country: country,
                city: city,
                street, street
            }
        }, (err, doc) => {
            if(!err) {
                res.status(200)
                .json({
                    message: 'User successfully updated'
                })
            }
        })
    } catch (err) {
        console.log(err)
    }

}