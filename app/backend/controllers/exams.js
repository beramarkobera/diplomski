const ExamDate = require('../models/examdate');
const Exam = require('../models/exam');

exports.getExamDates = async (req, res, next) => {
    console.log('Fetching dates...')
    let examDates = [];
    try {
        examDates = await ExamDate.find({ })
    } catch (error) {
        console.log(error);
    }

    console.log(examDates)
    res.status(200)
        .json({
            examDates: examDates
        })
}

exports.getExams = async (req, res, next) => {
    console.log('Fetching exams...')

    let exams = [];
    try {
        exams = await Exam.find({})
    } catch (error) {
        console.log(error);
    }

    console.log(exams)
    res.status(200)
        .json(exams)
}

exports.postSubmitApplication = async (req, res, next) => {
    console.log('POST SUBMIT')
    console.log(req.body);
    const {sub_id, user_email, schedule_id} = req.body;

    const exam = new Exam({
        subject: sub_id,
        userEmail: user_email,
        scheduleID: schedule_id,
        dateTime: new Date()
    })

    try {
        await exam.save();
        res.status(200)
    } catch (err) {
        console.log(err);
        res.status(400)
    }

    
}