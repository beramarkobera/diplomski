const Subject = require('../models/subject');

exports.getSubject = async (req, res, next) => {
	const subject_id = req.query.subject;
	let subject = null;

	try {
		subject = await Subject.findOne({ sub_id: subject_id})
	} catch (err) {
		console.log(err)
	}

	console.log('Found subject:')
	console.log(subject)

	res.status(200)
		.json({
			subject: subject
		})
	
};
