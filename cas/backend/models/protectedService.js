const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const protectedServiceSchema = new Schema({
    serviceURL: {
        type: String,
        required: true,
        unique: true
    }
});

module.exports = mongoose.model('ProtectedService', protectedServiceSchema);