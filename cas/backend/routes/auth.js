const express = require('express');

const User = require('../models/user.js');
const authController = require('../controllers/auth.js');

const router = express.Router();

router.post('/login', authController.login);
router.get('/serviceValidate', authController.validateTicket);
router.post('/validateCookie', authController.validateCookie);

router.get('/test', authController.getTest)

module.exports = router;