const fs = require("fs"); // file system
const https = require("https"); // creates an https server
const path = require("path");

const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);

const authRoutes = require('./routes/auth.js');

const MONGO_URL = 'mongodb://marko:maki1993@ds123465.mlab.com:23465/cas';

const privateKey = fs.readFileSync('localhost.key');
const certificate = fs.readFileSync('localhost.crt');

const app = express();

app.use(bodyParser.json());
app.use(cookieParser());

var store = new MongoDBStore({
    uri: MONGO_URL,
    collection: 'sessions'
});

app.use(
    session({ 
        secret: 'some/kInd8678OF[]a$#%SeCr37', 
        cookie: { 
            maxAge: 1000*60*60*8, // 8 hours    
            //maxAge: 1000*20,
            //sameSite: 'strict',
        },
        resave: false, 
        saveUninitialized: false,
        store: store
    })
  );

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
    //res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader(
        'Access-Control-Allow-Methods',
        'OPTIONS, GET, POST, PUT, PATCH, DELETE'
    );
    res.setHeader('Access-Control-Allow-Credentials', true);
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});

app.use('/auth', authRoutes);

mongoose
    .connect(MONGO_URL, { useNewUrlParser: true })
    //.connect('mongodb://localhost:27017/cas', { useNewUrlParser: true })
    .then(result => { 
        //app.listen(8080) 
        //https.createServer(options, app).listen(8080)
        https
            .createServer({ key: privateKey, cert: certificate}, app)
            .listen(8080)
    })
    .catch(err => console.log(err));

mongoose.connection.on('connected', () => {
    console.log('connected to mongodb');
})
mongoose.connection.on('error', err => {
    console.log('Error at mongoDB: ' + err);
})