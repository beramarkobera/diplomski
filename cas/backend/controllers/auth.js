const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const Verification = require('../models/verification');
const User = require('../models/user');
const ProtectedService = require('../models/protectedService');

const TGS_SECRET_KEY = 'fjioifiweffhpffncinwqe';
const TGT_SECRET = 'encodeTicketGrantingTicket';
const ST_SECRET = 'secretForMyServiceTicket';

exports.login = async (req, res, next) => {
    const serviceURL = decodeURI(req.body.params.split('service=')[1]);

    let foundUser = null;
    try {
        foundUser = await User.findOne({ email: req.body.email });
        if(!foundUser) {
            return res.json({ wrong: 'Wrong email or password'})
        }   
    } catch (err) {
        return res.json({ error: err });
    }

    let passwordCorrect;
    try {
        passwordCorrect = await bcrypt.compare(req.body.password, foundUser.password)      
    } catch (err) {
        return res.json({ error: err });
    }

    if(!passwordCorrect)
        res.json({ wrong: 'Wrong email or password!'})

    
    console.log('---------------WE MADE IT INTO USERS!')
    let userRegisteredToService = null;
    try {
        userRegisteredToService = await Verification.findOne({ email: foundUser.email, appURL: serviceURL })
    } catch (err) {
        return res.json({ error: 'Something wrong on catching user registered to service' })
    }

    if(!userRegisteredToService) {
        return res.json({ wrong: 'User with provided credentials is not Authorized to view requested service' })
    }

    let st = await issueST(serviceURL)

    const forwarded = req.headers['x-forwarded-for']
    const ip = forwarded ? forwarded.split(/, /)[0] : req.connection.remoteAddress;
    const userAgent = req.headers['user-agent'];
    const tgt = await issueTGT(foundUser.email, ip, userAgent)

    req.session.CASTGC = 'TGT-' + tgt;

    res.status(200)           
        .json({
            message: 'Valid user',
            ticket: 'ST-' + st,
            serviceUrl: serviceURL
        });
    
}

exports.validateTicket = async (req, res, next) => {
    const serviceURL = req.headers.origin;

    const jwtst = req.query.ticket.split('ST-')[1];
    let msg = await verifyST(jwtst, serviceURL)

    if(!msg) {
        res.status(400)
            .json({ msg: 'Service ticket is not valid' })
    }
    
    let decodedTGT = null;
    try {
        console.log('Decoding . . . ')
        await jwt.verify(req.session.CASTGC.split('TGT-')[1], TGS_SECRET_KEY, (err, decoded) => {
            if(!decoded || err) {
                res.status(400)
                    .json({ msg: 'Something went wrong during service ticket decoding'})
            }
            decodedTGT = decoded;
        })
    } catch (err) {
        res .status(500)
            .json({ msg: 'Server error'})
    }
    console.log('Logging decoded TGT data')
    console.log(decodedTGT)
    res.cookie('SSOSESSION', decodedTGT)
    res.status(200)
        .json({ msg: decodedTGT })
}

exports.validateCookie = async (req, res, next) => {   
    if(!req.session.CASTGC) {
        res.json({ wrong: 'There is no active SSO session'})
    }

    let decodedTGT = null;
    try {
        await jwt.verify(req.session.CASTGC.split('TGT-')[1], TGS_SECRET_KEY, (err, decoded) => {
            if(!decoded || err) {
                res.status(400)
                    .json({ msg: 'Something went wrong during service ticket decoding' })
            }
            decodedTGT = decoded;
            console.log('Logging decoded TGT data')
        })
    } catch (err) {
        res.status(500)
            .json({ msg: 'Server error' })
    }
    //console.log(decodedTGT)
    //console.log(req.cookies.SSOSESSION)
    if(decodedTGT || req.cookies.SSOSESSION) {
        if(decodedTGT.username === req.cookies.SSOSESSION.username) {
            const serviceURL = decodeURI(req.body.params.split('service=')[1]);
            res.status(200)
                .json({
                    message: 'Valid user',
                    serviceUrl: serviceURL
                })
        } 
    } else {
        res.json({ msg: 'Bad, something bad' })
    }

}

const issueTGT = async (username, ip, userAgent) => {
    const tgt = jwt.sign({
            username: username,
            ip: ip,
            userAgent: userAgent
        },
        TGS_SECRET_KEY,
        { expiresIn: 1000*60*60*8 }
    );
    return tgt;
}

const issueST = async (service) => {
    let protectedService = null;
    try {
        protectedService = await ProtectedService.findOne({ serviceURL: service })    
    } catch (err) {
        return { error: 'Something went wrong' }
    }

    if(!protectedService) {
        return {error: 'Requested service is not recognized' }
    }

    const st = jwt.sign(
        {
            serviceTicket: protectedService._id.toString()
        },
        ST_SECRET,
        { expiresIn: 20 }
    );
    return st;
}

const verifyST = async (serviceTicket, serviceURL) => {
    let decodedST = null;
    try {
        console.log('Decoding . . . ')
        await jwt.verify(serviceTicket, ST_SECRET, (err, decoded) => {
            if(!decoded || err) {
                return { msg: 'Something went wrong' }
            }
            decodedST = decoded;
        })
    } catch (err) {
        return { msg: err }
    }
    console.log(decodedST)
    let protectedService = null;
    try {
        protectedService = await ProtectedService.findById(decodedST.serviceTicket.toString())   
    } catch (err) {
        return { error: 'Something went wrong in verify ST' }
    }

    if(protectedService.serviceURL === serviceURL) {
        return true
    } else {
        return false
    }
}

exports.getTest = (req, res, next) => {
    res.json({
        test: 'test data'
    })
}

