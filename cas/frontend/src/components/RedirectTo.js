import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as ac from '../store/actions';


class RedirectTo extends Component {
    componentDidMount() {
        this.props.redirect(this.props.ticket, this.props.url);
    }
    render() {
        return (
            <div>
                <h1>Redirecting...</h1>
                <h3>{this.props.ticket}</h3>
                <h3>{this.props.url}</h3>
            </div>
        )
    }   
}

const mapStateToProps = state => {
    return {
        ticket: state.login.ticket,
        url: state.login.serviceUrl
    }   
}
const mapDispatchToProps = dispatch => {
    return {
        redirect: (ticket, url) => dispatch(ac.redirectToService(ticket, url))
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (RedirectTo);
