import React from 'react';
import FormElement from './FormElement/FormElement';

import {connect} from 'react-redux';
import * as ac from '../../store/actions';

import './Form.css';

const FormSignup = () => {
    return (
        <div>
            <form>
                <FormElement 
                    type='text'
                    placeholder='Email...'
                    name='email'
                    id='email'
                />
                <FormElement 
                    type='password'
                    placeholder='Password...'
                    name='password'
                    id='password'
                />
                <button> 
                    Signup
                </button>
            </form>
        </div>
    )
}

export default FormSignup
