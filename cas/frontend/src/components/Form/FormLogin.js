import React, {Component} from 'react';
import FormElement from './FormElement/FormElement';

import {connect} from 'react-redux';
import * as ac from '../../store/actions';

import './Form.css';

class FormLogin extends Component {

    render() {
        return (
            <div className='Form container'>
                <form onSubmit={(e) => this.props.handleSubmit(e, this.props.data, this.props.location.search)}>
                    <FormElement 
                        type='text'
                        placeholder='Email...'
                        name='email'
                        id='email'
                        value={this.props.data.email}
                    />
                    <FormElement 
                        type='password'
                        placeholder='Password...'
                        name='password'
                        id='password'
                        value={this.props.data.password}
                    />
                    <button> 
                        Login
                    </button>
                </form>
            </div>           
        )
    }
    
}

const mapStateToProps = state => {
    return {
        data: {
            email: state.login.email,
            password: state.login.password,
            submited: state.login.success
        }
    }
}

const mapDispatchToProps = dispatch => {
    return {
        handleSubmit: (e, data, params) => dispatch(ac.postSubmit(e, data, params))
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (FormLogin);
