import React from 'react';
import './FormElement.css';

import {connect} from 'react-redux';
import * as actionCreators from '../../../store/actions';

const FormElement = (props) => {

    const {type, placeholder, name, id} = props;
    return (
        <div className='Element wrapper'>
            <input 
                type={type}
                placeholder={placeholder}
                name={name}
                id={id}

                onChange={(e) => props.handleChange(name, e.target.value)}
            />
        </div>
    )
}

const mapDispatchToProps = dispatch => {
    return {
        handleChange: (name, value) => dispatch(actionCreators.handleChange(name, value))
    }
}

export default connect(null, mapDispatchToProps) (FormElement);
