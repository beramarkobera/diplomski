import React from 'react';
import {Link} from 'react-router-dom';
import './header.css';

const Header = () => {
    return (
        <div className='header right'>
            <Link to='/cas/login' className='header link'> Login </Link>
            <Link to='/cas/signup' className='header link'> Signup </Link>
        </div>
    )
}

export default Header
