import React, {Component} from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import {connect} from 'react-redux';
import * as actionCreators from './store/actions';

import { withRouter } from 'react-router'

import './App.css';
import Header from './components/Header/Header';
import FormLogin from './components/Form/FormLogin';
import FormSignup from './components/Form/FormSignup';
import RedirectTo from './components/RedirectTo';

class App extends Component {
	componentDidMount() {
		console.log(this.props.location.search)
		this.props.onCookieValidation(this.props.location.search);
	}

	render() {
		const alternateRoute = this.props.data.submited ? <Route path='/' component={RedirectTo} /> : null
		// TODO Remove comments
		return (
			<div className="App">
					<Header />
					<Switch>
						{alternateRoute}
						<Route path='/cas/login' component={FormLogin} />
						<Route path='/cas/signup' component={FormSignup} />
						<Route path='/cas/redirect' component={RedirectTo} />
						<Route exact path='/' component={FormLogin} />
					</Switch>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		data: {
            email: state.login.email,
            password: state.login.password,
            submited: state.login.success
        }
	}
}

const mapDispatchToProps = dispatch => {
	return {
		onCookieValidation: (params) => dispatch(actionCreators.validateSessionCookie(params))
	}
}

export default connect(mapStateToProps, mapDispatchToProps) (withRouter(App));
