import * as at from './actionTypes';
import {CAS_SERVER_URL} from '../constants';

export const handleChange = (name, val) => {
    return {
        type: at.CHANGE_FORM_ELEM,
        name: name,
        value: val
    }
};

export const getServiceUrl = (url) => {
    return {
        type: at.GET_SERVICE_URL,
        payload: url
    }
}
export const handleSubmit = (response) => {
    return {
        type: at.LOGIN_SUBMIT_SUCCESS,
        payload: response
    }
}
export const startLogin = () => {
    console.log('Login started...')
    return {
        type: at.LOGIN_SUBMIT_STARTED
    }
}
export const loginFailed = () => { 
    return { 
        type: at.LOGIN_SUBMIT_FAIL 
    }
}
export const redirectBack = () => {
    return {
        type: at.REDIRECT_BACK_TO_SERVICE
    }
} 

export const postSubmit = (e, data, params) => {
    const wholeData = {
        ...data,
        params
    }

    return async (dispatch) => {
        try {
            dispatch(startLogin());
            e.preventDefault();

            let response = null;
            let data = null;
            console.log('LOGIN TRY')
            response = await fetch(CAS_SERVER_URL + '/auth/login', {
                method: 'POST',
                credentials: 'include',
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
            },
                body: JSON.stringify(wholeData)
            });
            data = await response.json();
            console.log('DATA FROM LOGIN RESPONSE 2: ', data)
            if(data.message) {
                dispatch(getServiceUrl(data.serviceUrl))
                dispatch(handleSubmit(data));
            }

            if(data.wrong)
                dispatch(loginFailed());
        } catch (err) {
            console.log(err)
        }
        
    }
}


export const redirectToService = (ticket, url) => {
    const redirectParams = url + '?ticket=' + ticket;

    console.log(redirectParams);

    return dispatch => {
        dispatch(redirectBack());
        return window.location.assign(redirectParams)
    }
}

export const validateSessionCookie = (params) => {
    const pars = {
        params
    }
    return async dispatch => {
        let response = null;
        let data = null;

        try {
            response = await fetch(CAS_SERVER_URL + '/auth/validateCookie', {
                method: 'POST',
                //credentials: 'include',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(pars)
            });
            data = await response.json();

            if(data.message) {
                dispatch(getServiceUrl(data.serviceUrl))
                dispatch(handleSubmit(data));
            }

            if(data.wrong)
                dispatch(loginFailed());
                
        } catch (err) {
            console.log(err)
        }
    }
}
