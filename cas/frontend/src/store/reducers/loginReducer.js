import * as at from '../actions/actionTypes';

const initialState = {
    email: '',
    password: '',
    serviceUrl: null,
    ticket: null,
    success: false
}

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case at.CHANGE_FORM_ELEM: {
            return {
                ...state,
                [action.name] : action.value 
            }
        }
        case at.GET_SERVICE_URL: {
            console.log('get service URL requesting CAS')
            return {
                ...state,
                serviceUrl: action.payload
            }
        }
        case at.LOGIN_SUBMIT_STARTED: {
            console.log('login started')
            return {
                ...state,
                success: false
            }
        }
        case at.LOGIN_SUBMIT_SUCCESS: {
            const succ = !state.success;
            return {
                ...state,
                success: succ,
                ticket: action.payload.ticket
            }
        }
        case at.LOGIN_SUBMIT_FAIL: {
            console.log('login failed')
            return {
                ...state,
                success: false
            }
        }
        default:
            return state;
    }
}



export default reducer;